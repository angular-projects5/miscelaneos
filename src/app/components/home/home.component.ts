import {
  Component, OnInit, OnChanges, DoCheck, AfterContentInit,
  AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy
} from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <app-ng-style></app-ng-style>
  <app-css></app-css>
  <p>
    Hola mundo desde appcomponent
  </p>


  <app-clases></app-clases>

  <br><br>
  <p [appResaltado]="'red'">
    Texto resaltado por directiva
  </p>
  <br><br>
  <app-ng-switch></app-ng-switch>
  `,
  styles: []
})
export class HomeComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

  constructor() {
    console.log('constructor');
  }

  ngOnInit(): void {
    // Cuando el componente está inicializando
    console.log('Segundo ciclo de vida: ngOnInit');
  }

  ngOnChanges() {
    // cuando los datos de propiedades relacionadas cambian
    console.log('Primer ciclo de vida: ngOnChanges');
  }
  ngDoCheck() {
    // Durante cada revisión del ciclo de detección de cambios
    console.log('Tercer ciclo de vida: ngDoCheck');
  }
  ngAfterContentInit() {
    // Después de insertar contenido (<app-alguna-pagina>)
    console.log('Cuarto ciclo de vida: ngAfterContentInit');
  }
  ngAfterContentChecked() {
    // Después de la revisión del contenido insertado
    console.log('Quinto ciclo de vida: ngAfterContentChecked');
  }
  ngAfterViewInit() {
    // Después de la inicialización del copmonente/hijos
    console.log('Sexto ciclo de vida: ngAfterViewInit');
  }
  ngAfterViewChecked() {
    // Después de cada revisión de los componentes/hijos
    console.log('Séptimo ciclo de vida: ngAfterViewChecked');
  }
  ngOnDestroy() {
    // Justo antes de que se destruya el componente o directiva
    console.log('Octavow ciclo de vida: ngOnDestroy');
  }

}
