import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-switch',
  templateUrl: './ng-switch.component.html',
  styles: []
})
export class NgSwitchComponent implements OnInit {

  values: string[] = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'alert-light'];

  alerta = 'warning';
  constructor() { }

  ngOnInit(): void {
  }

  cambiarAlerta() {
    const index = Math.floor(Math.random() * 6) + 1;

    this.alerta = this.values[index];
  }

}
