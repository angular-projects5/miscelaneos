import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-editar',
  template: `
    <p>
      El parámetro id del padre es: {{padreId}}
    </p>
  `,
  styles: []
})
export class UsuarioEditarComponent implements OnInit {

  padreId: number;

  constructor(private router: ActivatedRoute) {
    this.router.parent.params.subscribe(parametros => {
      console.log('Ruta hija de editar apuntando al padre');
      console.log(parametros);
      this.padreId = parametros.id;
    });
  }

  ngOnInit(): void {
  }

}
