import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-nuevo',
  template: `
    <p>
      El parámetro recibido por ruta en el componente hijo es: {{parametroHijo}}
    </p>
  `,
  styles: []
})
export class UsuarioNuevoComponent implements OnInit {

  parametroHijo: string;

  constructor(private router: ActivatedRoute) {
    this.router.params.subscribe(parametros => {
      console.log('Ruta hija usuario nuevo');
      console.log(parametros);
      this.parametroHijo = parametros.parametro2;
    });
  }

  ngOnInit(): void {
  }

}
